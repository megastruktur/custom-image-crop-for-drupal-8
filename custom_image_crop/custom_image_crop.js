/**
 * Initialize jCrop and process it's behaviors
 */

(function ($, Drupal) {

  "use strict";

// global var:
  var cic_image_style_class = 'image-style-cic-style';
  var selector = '.' + cic_image_style_class;
  
  // selecting the init image width to fit some bounds:
  var width_initimage = 350;

  var USE_FACE_DETECT = true;

  /**
   * Attaching an image crop library.
   */
  Drupal.behaviors.customImageCropAttach = {
    attach: function (context, settings) {

      // initial image:
      var $initimage = $(selector);

      // checking, if element exists AND if already jCropped:
      if ($initimage.length > 0 && !$initimage.attr('jcropped')) {

        // defining elements:
        var _cic_field_x = $('input[name$="cic_x]"]');
        var _cic_field_y = $('input[name$="cic_y]"]');
        var _cic_field_w = $('input[name$="cic_w]"]');
        var _cic_field_h = $('input[name$="cic_h]"]');

        var cic_fieldname = $('input[name$="cic_field]"]').val().replace(/^field_/, "");
        var cic_fieldpath = $('input[name$="cic_path]"]').val();

        $initimage.attr('jcropped', 'jcropped');

        // adding an image src to preview pane
        // through JS, coz Drupal processes russian filenames incorrectly
        $('<div id="preview-pane"><div class="preview-container"><img src="' + $initimage.attr('src') + '" class="jcrop-preview" alt="Preview" /></div></div>')
                .insertAfter($initimage);

        // calculating ratio:
        var initimage_ratio = $initimage.width() / width_initimage;
        // setting the css style for init image to fit some bounds:
        $initimage.css({
          width: width_initimage + 'px',
          height: 'auto'
        });

        // Create variables (in this scope) to hold the API and image size
        var jcrop_api,
                boundx,
                boundy,
                // Grab some information about the preview pane
                $preview = $('#preview-pane'),
                $pcnt = $('#preview-pane .preview-container'),
                $pimg = $('#preview-pane .preview-container img'),
                xsize = $pcnt.width(),
                ysize = $pcnt.height(),
                // initial selection bounds:
                def_x = Math.round(parseInt(_cic_field_x.val()) / initimage_ratio),
                def_y = Math.round(parseInt(_cic_field_y.val()) / initimage_ratio),
                def_w = Math.round(parseInt(_cic_field_w.val()) / initimage_ratio),
                def_h = Math.round(parseInt(_cic_field_h.val()) / initimage_ratio);

        if (USE_FACE_DETECT && (!def_w || !def_h)) {


          // making AJAX to define the face coordinates (CALL A FACE DETECT):
          //// BE SURE TO CHECK the file--image selector, possible the file--<fieldname>
          var filename = $('.file.file--image').text().replace(/\s+/g, '');

          var myUrl = '/face_detect';
          $
                  .ajax({
                    url: myUrl,
                    method: "GET",
                    dataType: 'json',
                    data: {
                      filename: filename,
                      field: cic_fieldname,
                      path: cic_fieldpath
                    }
                  })
                  .done(function (data) {

                    // if success - add corresponding coordinates:
                    if (data.code === 0) {
                      def_x = Math.round(data.face.x / initimage_ratio);
                      def_y = Math.round(data.face.y / initimage_ratio);
                      def_w = Math.round(data.face.w / initimage_ratio);
                      def_h = Math.round(data.face.h / initimage_ratio);
                    }
                    // initializing the crop:
                    jCropBind($initimage);

                  });
          // else - fire without AJAX:
        } else {
          jCropBind($initimage);
        }

      }


      /**
       * Function binds a jCrop to selector
       * @param {DOM} $initimage
       * @returns {undefined}
       */
      function jCropBind($initimage) {
        $initimage
                .Jcrop({
                  onChange: updatePreview,
                  onSelect: updatePreview,
                  aspectRatio: xsize / ysize
                }, function () {
                  // Use the API to get the real image size
                  var bounds = this.getBounds();
                  boundx = bounds[0];
                  boundy = bounds[1];
                  // Store the API in the jcrop_api variable
                  jcrop_api = this;

                  // Move the preview into the jcrop container for css positioning
                  $preview.appendTo(jcrop_api.ui.holder);
                  jcrop_api.setSelect([def_x, def_y, def_w + def_x, def_h + def_y]);
                });
      }

      /**
       * Function updates preview
       * @param {object} c
       * @returns {undefined}
       */
      function updatePreview(c) {
        if (parseInt(c.w) > 0) {

          var rx = xsize / c.w;
          var ry = ysize / c.h;

          var cic_width = Math.round(rx * boundx) + 'px';
          var cic_height = Math.round(ry * boundy) + 'px';
          var cic_marginLeft = '-' + Math.round(rx * c.x) + 'px';
          var cic_marginTop = '-' + Math.round(ry * c.y) + 'px';

          // preview pane adjusting:
          $pimg.css({
            width: cic_width,
            height: cic_height,
            marginLeft: cic_marginLeft,
            marginTop: cic_marginTop
          });

          // input fields filling:
          _cic_field_x.val(c.x * initimage_ratio);
          _cic_field_y.val(c.y * initimage_ratio);
          _cic_field_w.val(c.w * initimage_ratio);
          _cic_field_h.val(c.h * initimage_ratio);
        }
      }

    },
    detach: function (context, settings, trigger) {
      // TODO: write a detach, if necessary
    }
  };
})(jQuery, Drupal);