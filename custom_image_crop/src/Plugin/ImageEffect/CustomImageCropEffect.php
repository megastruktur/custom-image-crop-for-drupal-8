<?php

/**
 * @file
 * Contains \Drupal\mcrop\Plugin\ImageEffect\ManualCropCropImageEffect.
 */

namespace Drupal\custom_image_crop\Plugin\ImageEffect;

use Drupal\image\ImageEffectBase;
use Drupal\Core\Image\ImageInterface;

/**
 * Scales and crops an image resource while keeping its manual crop as close to
 * the center of the resulting image as possible.
 *
 * @ImageEffect(
 *   id = "custom_image_crop",
 *   label = @Translation("Custom Image Crop"),
 *   description = @Translation("Crop the image with selection from DB")
 * )
 */
class CustomImageCropEffect extends ImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {

    $images_src = $image->getSource();

    // retrieving coordinates from DB and cropping:
    if ($images_src) {
      $coordinates = $this->_cic_return_image_coords($images_src);
      if ($coordinates) {
        $image->crop($coordinates['x'], $coordinates['y'], $coordinates['w'], $coordinates['h']);
      }
    }

    return TRUE;
  }

  /**
   * Will return coords by image's cource address
   * 
   * @param string $images_src
   * @return array OR bool
   */
  public function _cic_return_image_coords($images_src) {

    $query = db_select('file_managed', 'f');
    $query->join('custom_image_crop', 'cic', 'f.fid = cic.fid');
    $query->fields('cic', array('x', 'y', 'w', 'h'))
            ->condition('f.uri', $images_src, '=');
    return $query->execute()
                    ->fetchAssoc();
  }

}
