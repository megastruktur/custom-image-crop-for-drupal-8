<?php

/**
 * @file
 * Contains \Drupal\face_detect\Controller\FaceDetectController
 */

namespace Drupal\face_detect\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\face_detect\Plugin\php_facedetection\FaceDetectorExtended;

class FaceDetectController {

        // directory inside the sites/default/files (use / at the end):
        public $image_dir = 'field/image/';

        // padding
        public $padding = 0;
        
        /**
         * Give me the filename - I'll detect the face
         * 
         * @param \Drupal\face_detect\Controller\Request $request
         * @return JsonResponse
         */
        public function content(Request $request) {

                // retrieving data:
                $filename = $request->get('filename', '');
                $fieldname = $request->get('field', '');
                $fieldpath = $request->get('path', '');
                
                // fieldname path settings:
                //// !!!! CHECK if the dir is fieldname OR widget specific !!!!
                if ($fieldname && $fieldpath) {
                  $this->image_dir = $fieldpath . '/';
                }

                // creating vars:
                $return = array();

                // file not specified:
                if (!$filename) {
                        $return['code'] = 1;
                        $return['message'] = 'Filename not specified';
                        return new JsonResponse($return);
                }

                // searching for the file:
                $file_uri = 'public://' . $this->image_dir . $filename;
                $real_path = file_stream_wrapper_get_instance_by_uri($file_uri)->realpath();

                if (!file_exists($real_path)) {
                        $return['code'] = 2;
                        $return['message'] = 'File not found';
                        $return['path'] = $real_path;
                        return new JsonResponse($return);
                }

                //// <---- Detecting the face:
                $oFaceDetectorExtended = new FaceDetectorExtended();
                $coords_raw = $oFaceDetectorExtended->faceDetect($real_path);

                // retrieving coords with correct paddings:
                if ($coords_raw && $coords_raw['w'] > 0) {

                        $coords = $oFaceDetectorExtended->getFaceCoordinates($this->padding);

                        if ($coords) {
                                $return['code'] = 0;
                                $return['message'] = 'Success';
                                $return['face'] = $coords;
                                return new JsonResponse($return);
                        }
                }
                //// ---->

                // the face was not detected:
                if (!$coords_raw || !$coords_raw['w'] || !$coords) {
                        $return['code'] = 3;
                        $return['message'] = 'Not detected';
                        return new JsonResponse($return);
                }
        }

}
