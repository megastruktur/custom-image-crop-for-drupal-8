<?php

namespace Drupal\face_detect\Plugin\php_facedetection;

use Drupal\face_detect\Plugin\php_facedetection\FaceDetector;
use Exception;

class FaceDetectorExtended extends FaceDetector {

  // do we use a manual crop?
  protected $manualCrop = FALSE;
  // debug mode:
  protected $debug = FALSE;

  /**
   * 
   * @param array $aCustomCrop
   *    Can be empty, if autodetecting face
   *    ['x'] - x-coordinate of the beginning of ht selected area
   *    ['y'] - y-coordinate of the beginning of ht selected area
   *    ['w'] - width of the selected area
   *    ['h'] - height of the selected area (if empty or 0 - will be calculated automatically)
   */
  function __construct($aCustomCrop = array()) {
    parent::__construct();

    if (isset($_GET['debug'])) {
      $this->debug = TRUE;
    }

    // checking if custom data is provided:
    if ($aCustomCrop && isset($aCustomCrop['x']) && isset($aCustomCrop['y']) && isset($aCustomCrop['w']) && isset($aCustomCrop['w'])) {
      // clearing the attr and defining it with new values:
      unset($this->face);
      $this->face = $aCustomCrop;
      $this->manualCrop = TRUE;


      //// <---- DEBUG SECTION:
      if ($this->debug) {
        print_r($this->face['x'] . ' face x initial <br>');
        print_r($this->face['y'] . ' face y initial <br>');
      }
      //// ---->
    }
  }

  //override default function for face detection
  public function faceDetect($file) {
    if (is_resource($file)) {

      $this->canvas = $file;
    } elseif (is_file($file)) {

      $this->canvas = imagecreatefromjpeg($file);
    } else {

      throw new Exception("Can not load $file");
    }

    $im_width = imagesx($this->canvas);
    $im_height = imagesy($this->canvas);

    //Resample before detection?
    $resample_w = 320;
    $resample_h = 240;

    $diff_width = $resample_w - $im_width;
    $diff_height = $resample_h - $im_height;
    if ($diff_width > $diff_height) {
      $ratio = $im_width / $resample_w;
    } else {
      $ratio = $im_height / $resample_h;
    }

    if (!$this->manualCrop) {
      if ($ratio != 0) {
        $this->reduced_canvas = imagecreatetruecolor($im_width / $ratio, $im_height / $ratio);

        imagecopyresampled(
                $this->reduced_canvas, $this->canvas, 0, 0, 0, 0, $im_width / $ratio, $im_height / $ratio, $im_width, $im_height
        );

        $stats = $this->getImgStats($this->reduced_canvas);

        if (!$this->face) {
          $this->face = $this->doDetectGreedyBigToSmall(
                  $stats['ii'], $stats['ii2'], $stats['width'], $stats['height']
          );
        }

        if ($this->face['w'] > 0) {
          $this->face['x'] *= $ratio;
          $this->face['y'] *= $ratio;
          $this->face['w'] *= $ratio;
          $this->face['h'] = $this->face['w'];
        }
      } else {
        $stats = $this->getImgStats($this->canvas);

        //print_r($stats);

        if (!$this->face) {
          $this->face = $this->doDetectGreedyBigToSmall(
                  $stats['ii'], $stats['ii2'], $stats['width'], $stats['height']
          );
        }
      }
    }
    return $this->face;
  }

  /**
   *
   *   Custom function to resize face image
   */
  public function resizeFace($padding = 25, $header = TRUE) {

    //INIT COORDINATES AND SIZES
    $original_img_width = imagesx($this->canvas);
    $original_img_height = imagesy($this->canvas);

    $face_width = $this->face['w'];
    // we need square images, so:
    $face_height = $face_width;

    //Face height can't be more than image height
    if ($face_height > $original_img_height) {
      $face_height = $original_img_height;
    }

    $max_padding_top = $this->face['y'];
    $max_padding_bottom = $original_img_height - $this->face['y'] - $face_height;
    if ($max_padding_bottom < 0) {
      $max_padding_bottom = 0;
    }

    $max_padding_left = $this->face['x'];
    $max_padding_right = $original_img_width - $this->face['x'] - $face_width;
    if ($max_padding_right < 0) {
      $max_padding_right = 0;
    }

    //PADDING LOGIC: DISALLOW THEM IN CASE IF THERE IS NO SPACE BETWEEN FACE AND OR. IMAGE BORDER
    //change padding amount (if face height is almost the same size as image we don't add padding)
    //// FOR AUTO DETECT ONLY
    // using "0" values for manual crop:
    if (!$this->manualCrop) {
      for (
      $padding_to_add_if_possible_vertical = $padding; ($padding_to_add_if_possible_vertical > $max_padding_top) or ( $padding_to_add_if_possible_vertical > $max_padding_bottom); $padding_to_add_if_possible_vertical--
      ) {
        
      }

      //change padding amount (if face height is almost the same size as image we don't add padding)
      for (
      $padding_to_add_if_possible_horizontal = $padding; ($padding_to_add_if_possible_horizontal > $max_padding_left) or ( $padding_to_add_if_possible_horizontal > $max_padding_right); $padding_to_add_if_possible_horizontal--
      ) {
        
      }
    } else {
      $padding_to_add_if_possible_vertical = $padding_to_add_if_possible_horizontal = $padding = 0;
    }

    //// <---- DEBUG SECTION:
    if ($this->debug) {
      print_r($face_width . ' face_width <br>');
      print_r($face_height . ' face_height <br>');
      print_r($original_img_width . ' original_img_width <br>');
      print_r($original_img_height . ' original_img_height <br>');
      print_r($padding_to_add_if_possible_horizontal . ' padding_to_add_if_possible_horizontal <br>');
      print_r($padding_to_add_if_possible_vertical . ' padding_to_add_if_possible_vertical <br>');
      print_r($this->face['x'] . ' face x <br>');
      print_r($this->face['y'] . ' face y <br>');
    }
    //// ---->

    $canvas = imagecreatetruecolor($face_width + ($padding_to_add_if_possible_horizontal * 2), $face_height + ($padding_to_add_if_possible_vertical * 2));

    //int imagecopyresized (resource dst_im, resource src_im, int dstX, int dstY, int srcX, int srcY, int dstW, int dstH, int srcW, int srcH)  
    /*
      imagecopyresampled(
      //init new image
      $canvas,
      //face image to cut
      $this->canvas,
      //margin left-right
      $dstX = 0,
      //margin top-bottom
      $dstY = 0,
      //image starts from left coordinate
      $srcX = $this->face['x'] - $padding_to_add_if_possible_horizontal,
      //image starts from top coordinate
      $srcY = $this->face['y'] - $padding_to_add_if_possible_vertical,
      //
      $dstW = $face_width + ($padding_to_add_if_possible_horizontal * 2 ),
      //
      $dstH = $dstW,
      //
      $srcW = $face_width + ($padding_to_add_if_possible_horizontal * 2 ),
      //
      $srcH = $srcW
      );
     */
    if (!$this->manualCrop) {
      imagecopyresized(
              //init new image
              $canvas,
              //face image to cut
              $this->canvas,
              //margin left-right
              $dstX = 0,
              //margin top-bottom
              $dstY = 0,
              //image starts from left coordinate
              $srcX = $this->face['x'] - $padding_to_add_if_possible_horizontal,
              //image starts from top coordinate
              $srcY = $this->face['y'] - $padding_to_add_if_possible_vertical, $dstW = $face_width + ($padding_to_add_if_possible_horizontal * 2 ), $dstH = $face_height + ($padding_to_add_if_possible_vertical * 2 ), $srcW = $face_width + ($padding_to_add_if_possible_horizontal * 2 ), $srcH = $face_height + ($padding_to_add_if_possible_vertical * 2 )
      );
    } else {
      imagecopyresized(
              //init new image
              $canvas,
              //face image to cut
              $this->canvas,
              //margin left-right
              $dstX = 0,
              //margin top-bottom
              $dstY = 0,
              //image starts from left coordinate
              $srcX = $this->face['x'],
              //image starts from top coordinate
              $srcY = $this->face['y'], $dstW = $face_width, $dstH = $face_height, $srcW = $face_width, $srcH = $face_height
      );
    }


    if ($header) {
      $this->_outImage($canvas);
    } else {
      return $canvas;
    }
  }

  /**
   *
   *   Retrieving face coordinates with padding
   */
  public function getFaceCoordinates($padding = 25) {

    //INIT COORDINATES AND SIZES
    $original_img_width = imagesx($this->canvas);
    $original_img_height = imagesy($this->canvas);

    $face_width = $this->face['w'];
    // we need square images, so:
    $face_height = $face_width;

    //Face height can't be more than image height
    if ($face_height > $original_img_height) {
      $face_height = $original_img_height;
    }

    $max_padding_top = $this->face['y'];
    $max_padding_bottom = $original_img_height - $this->face['y'] - $face_height;
    if ($max_padding_bottom < 0) {
      $max_padding_bottom = 0;
    }

    $max_padding_left = $this->face['x'];
    $max_padding_right = $original_img_width - $this->face['x'] - $face_width;
    if ($max_padding_right < 0) {
      $max_padding_right = 0;
    }

    //PADDING LOGIC: DISALLOW THEM IN CASE IF THERE IS NO SPACE BETWEEN FACE AND OR. IMAGE BORDER
    //change padding amount (if face height is almost the same size as image we don't add padding)
    //// FOR AUTO DETECT ONLY
    // using "0" values for manual crop:
    if (!$this->manualCrop) {
      for (
      $padding_to_add_if_possible_vertical = $padding; ($padding_to_add_if_possible_vertical > $max_padding_top) or ( $padding_to_add_if_possible_vertical > $max_padding_bottom); $padding_to_add_if_possible_vertical--
      ) {
        
      }

      //change padding amount (if face height is almost the same size as image we don't add padding)
      for (
      $padding_to_add_if_possible_horizontal = $padding; ($padding_to_add_if_possible_horizontal > $max_padding_left) or ( $padding_to_add_if_possible_horizontal > $max_padding_right); $padding_to_add_if_possible_horizontal--
      ) {
        
      }
    } else {
      $padding_to_add_if_possible_vertical = $padding_to_add_if_possible_horizontal = $padding = 0;
    }

    $canvas = imagecreatetruecolor($face_width + ($padding_to_add_if_possible_horizontal * 2), $face_height + ($padding_to_add_if_possible_vertical * 2));

    $coordinates = array(
            'x' => $this->face['x'] - $padding_to_add_if_possible_horizontal
            , 'y' => $this->face['y'] - $padding_to_add_if_possible_vertical
            , 'w' => $face_width + ($padding_to_add_if_possible_horizontal * 2 )
            , 'h' => $face_height + ($padding_to_add_if_possible_vertical * 2 )
    );

    return $coordinates;
  }

  private function _outImage($canvas) {
    if (!headers_sent()) {
      header('Content-type: image/jpeg');
      imagejpeg($canvas);
    }
  }

}
