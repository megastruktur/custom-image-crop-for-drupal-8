#Custom Image Crop and Face Detection modules for Drupal 8(b11).
#Working together. FD depends on CIC.

###Server requirements
- PHP version >= 5

###General requirements
- Drupal 8 (beta 11)

###About
CIC (Custom image Crop) module allows admin to config an image field to use a jCrop library to crop images.
FD (Face Detect) module allows to detect face coordinates, based on ajax file request on /face_detect page.
All tech details are in comments inside the code

CIC depends on FD. To use CIC separately:
1. Remove dependency from custom_image_crop.info.yml file.
2. At custom_image_crop/custom_image_crop.js:
- find the str var USE_FACE_DETECT = true; and change it to var USE_FACE_DETECT = false;

###Installation process:
1. Go to Extend menu (/admin/modules).
2. Enable Custom Image Crop module.
3. You'll be prompted if you want to enable Face Detect module too. Click "Continue" and wat for modules to install.

###How to configure:
1. Go to desired Content Type OR, in our case, user fields management at /admin/config/people/accounts.
2. Go to "Manage Fields" tab and Edit the proper field (Picture field).
3. Scroll to the bottom of the config page, check the "Use custom crop" checbox and click "Save Settings". This will enable Custom Image Crop for that field.
**NOTE:** You also would like to modify picture dimensions AND image size. To do so just change (or remove for unlimit) corresponding fields: "Maximum image resolution" and "Maximum upload size".
4. Go to "Manage Display" tab, find the cog icon opposite the Picture field (cog icon is on the right side of the screen), click it.
5. Select the "CIC system style" and click "Update".
**NOTE:** You need "CIC system style" and NOT the "CIC module style".
6. Save the display settings by clicking "Save" button at the bottom.
**NOTE:** Don't forget to click "Savr" and save this config.

Now if you go to /user/1/edit and add your image to "Picture" field you'll be able to use custom cropping effect.
The resulting image is 250x250 pixels, and it can be configured at "CIC system style" image style, "scale" effect option.
